<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePivotTesti extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_testimonial', function (Blueprint $table) {
            //
            $table->dropColumn('course_id');
            $table->dropColumn('testimonial_id');
            $table->integer('courses_id');
            $table->integer('testimonials_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_testimonial', function (Blueprint $table) {
            //
        });
    }
}
