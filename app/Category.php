<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name', 'sort'

    ];

    //

    public function courses(){
        return $this->belongsToMany('App\Course', 'category_course',
            'categories_id', 'courses_id');
    }

}
