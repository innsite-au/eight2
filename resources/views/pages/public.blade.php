@extends('layouts.app')

@section('meta')
    <title>{{ $meta->title }}</title>
    <meta name="description" content="{{ $meta->description }}">
@endsection

@section('content')


    <!-- Banner
    ============================================= -->
    <section id="content ban"  alt="{{ $page->alt_tag }}" title="{{ $page->alt_tag }}"
             style="
                     background-image: url({{ $page->banner_image }});
                     background-position: center center;
                     background-size: cover;
                     ">
        <div class="upb_bg_overlay"></div>
        <div class="upb_bg_overlay_pattern"></div>
        <div class="banner">
            <div class="content-wrap">

                <div class="container clearfix">
                    <div class="col-md-12">
                        <div class="banner-center">
                            <h1>{{ $page->banner_title }}</h1>
                            <p>{{ $page->banner_text }}</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>


    <!-- Intro block
    ============================================= -->
    <section id="content">
        <div class="content-wrap" style="padding: 60px 0px 0px">
            <div class="container">
                <div class="row">

                    {{--Intro--}}
                    <div class="col-md-12">
                        {!! $page->body !!}
                    </div>

                </div>
            </div>
        </div>
    </section>


    <!-- Courses block
    ============================================= -->
    <section id="content"  style="background: #F5F5F5">
        <div class="content-wrap">
            <div class="container">
                <h2>{{ trans('academy.public.courses') }}</h2>
                <div class="row courses">
                    @foreach($courses as $course)
                        <div class="col-md-3">
                            <div class="c-image">
                                @if($course->icon)
                                    <img class="cicon" src="{{$course->icon}}" alt="{{$course->name}}" title="{{$course->name}}">
                                @else
                                    <img class="cicon" src="{{url('images/logo.png')}}" alt="{{$course->name}}" title="{{$course->name}}">
                                @endif
                            </div>
                            <div class="summary">

                                <h3>{{$course->name}}</h3>
                                <p>{!!   strip_tags(\Illuminate\Support\Str::limit($course->summary, 100)) !!}</p>
                                <a href="/public/{{$course->slug}}" class="button button-small">{{ trans('academy.view.course') }}</a>

                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>


    <!-- Dates Block
============================================= -->
    <section id="content">
        <div class="content-wrap">
            <div class="container">

                @include('layouts.dates')

            </div>
        </div>
    </section>



    <!-- Testimonials block
    ============================================= -->

    <section id="content" style="background: #fff;">
        <div class="content-wrap">
            @include('layouts.testimonial')
        </div>
    </section>


    <!-- Call to action block
        ============================================= -->
    <section id="content" style="background: #F5F5F5; text-align:center">
            @include('layouts.callto')
    </section>




@endsection

@section('scripts')

@endsection


