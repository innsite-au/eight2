<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('category_course', function (Blueprint $table) {
            //
            $table->string('categories_id');
            $table->string('courses_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('category_course', function (Blueprint $table) {
            //
            $table->string('category_id');
            $table->string('course_id');
        });
    }
}
