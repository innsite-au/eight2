<?php

namespace App\Http\Controllers;

use App\Date;
use App\Partner;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Course;
use DB;
use App\Banner;
use App\Testimonial;
use App\Client;
use App\Trainer;
use App\Category;
use App\Block;
use App\Title;


class CourseController extends Controller
{

    //get Academy page
    public function getAcademy(){
        $banner = Banner::findOrFail(3);
        $block = Block::findOrFail(1);
        $testimonials = Testimonial::orderBy('sort','asc')->get();
        $trainers = Trainer::orderBy('sort','asc')->get();
        $clients = Client::all();
        $partners = Partner::all();
        $meta = Title::findOrFail(1);
        $dates = Date::orderBy('start_date','ASC')->where('start_date', '>=', Carbon::now())->get();

        return view('pages.academy',
               compact('banner', 'block', 'meta'),
               ['testimonials' => $testimonials,
                 'clients' => $clients,
                 'trainers' => $trainers,
                 'partners' => $partners,
                   'dates' => $dates
               ]);
    }


    //get all public events
    public function getPublic(){
        $banner = Banner::findOrFail(1);
        $testimonials = Testimonial::orderBy('sort','asc')->get();
        $courses = Course::orderBy('sort','ASC')->where('type', '!=', 'Private')->get();
        $dates = Date::orderBy('start_date','ASC')->where('start_date', '>=', Carbon::now())->get();
        $meta = Title::findOrFail(2);

        return view('pages.public',
                compact('banner', 'meta'),
                [   'courses' => $courses,
                    'testimonials' => $testimonials,
                    'dates' => $dates ]);
    }


    //get all private events
    public function getPrivate(){
        $banner = Banner::findOrFail(2);
        $testimonials = Testimonial::orderBy('sort','asc')->get();
        $categories = Category::orderBy('sort','ASC')->get();
        $meta = Title::findOrFail(3);
        $courses = Course::orderBy('sort','ASC')->where('type', '!=', 'Public')->get();

        return view('pages.private',
            compact('banner', 'meta'),
            ['courses' => $courses,
                'testimonials' => $testimonials,
                'categories' => $categories]);

    }

    //get public course detail page
    public function getCourseDetail($slug){

        $course = Course::where('slug', '=', $slug)->first();
        $testimonials = Testimonial::orderBy('sort','asc')->get();

        return view('pages.course-detail',
                  compact('course'),
                  ['testimonials' => $testimonials,'course'=>$course]);


    }

    //get private course detail page
    public function getCourseDetailPrivate($slug){
        $course = Course::where('slug', '=', $slug)->first();
        $testimonials = Testimonial::orderBy('sort','asc')->get();

        return view('pages.private-course-detail',
            compact('course'),
            ['testimonials' => $testimonials,]);
    }



    // -- Arlo Integration
    // -- Arlo Templates = Courses table in this app
    // -- Arlo Courses = Sessions table in this app
    // -- When we Sync Arlo, first we will create new entry or update entry in the courses table
    //    from API https://elabor8.arlo.co/api/2012-02-01/pub/resources/eventtemplates
    //    TemplateID is the unique identifier saved in column event_id courses table
    // -- Then we will create new entry ot update entry in the sessions table
    //    from API https://elabor8.arlo.co/api/2012-02-01/pub/resources/eventsearch?fields=EventID,EventTemplateID,IsFull,IsPrivate,PlacesRemaining,RegistrationInfo,Name,Code,StartDateTime,EndDateTime,Location


    public function coursesUpdate()
    {
        $apiURL = 'https://elabor8.arlo.co/api/2012-02-01/pub/resources/eventtemplates';

        $client = new \GuzzleHttp\Client(['headers' => ['Accept' => 'application/json']]);
        $res = $client->request('GET', $apiURL);
        if ($res->getStatusCode() == 200) {
            $events = \GuzzleHttp\json_decode($res->getBody());

            if ($events->Count !== 0) {
                foreach ($events->Items as $event) {
                    $event_id = $event->TemplateID;
                    $name = $event->Name;


                    Course::updateOrCreate(
                        ['event_id' => $event_id],
                        [
                            'event_id' => $event_id,
                            'name' => $name,
                        ]
                    );
                }
                return back();

            } else {
                return 'Error. No Data available';
            }

        } else {
            return 'API access error.';
        }

    }



    public function datesUpdate()
    {
        $apiURL = 'https://elabor8.arlo.co/api/2012-02-01/pub/resources/eventsearch?fields=EventID,EventTemplateID,IsFull,IsPrivate,PlacesRemaining,RegistrationInfo,Name,Code,StartDateTime,EndDateTime,Location';

        $client = new \GuzzleHttp\Client(['headers' => ['Accept' => 'application/json']]);
        $res = $client->request('GET', $apiURL);
        if ($res->getStatusCode() == 200) {
            $dates = \GuzzleHttp\json_decode($res->getBody());


            if ($dates->Count !== 0) {
                foreach ($dates->Items as $date) {
                    $id = $date->EventID;
                    $courseId = $date->EventTemplateID;
                    $name = $date->Name;
                    $code = $date->Code;
                    $statDate = new \DateTime($date->StartDateTime);
                    $endDate = new \DateTime($date->EndDateTime);
                    $city = $date->Location->City;
                    $isFull =  $date->IsFull;
                    $PlacesRemaining = $date->PlacesRemaining;
                    $registerUri = $date->RegistrationInfo->RegisterUri;
                    $isPrivate = $date->IsPrivate;
                    $slug = str_slug($date->Name, '-');


                    Date::updateOrCreate(
                        ['event_id' => $id],
                        [
                            'event_id' => $id,
                            'name' => $name,
                            'code' => $code,
                            'start_date' => $statDate,
                            'end_date' => $endDate,
                            'city' => $city,
                            'is_full' => $isFull,
                            'places_remaining' => $PlacesRemaining,
                            'course_id' => $courseId,
                            'register_uri' => $registerUri,
                            'is_private' => $isPrivate,
                            'slug' => $slug

                        ]
                    );
                }
                return back();

            } else {
                return 'Error. No Data available';
            }
        } else {
            return 'API access error.';
        }
    }


}
