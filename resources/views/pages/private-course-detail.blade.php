@extends('layouts.app')

@section('meta')
    <title>{{$course->title_tag ? $course->title_tag : $course->name}}</title>
    <meta name="description" content="{{$course->meta_description}}">
@endsection

@section('content')

    <!-- Banner
    ============================================= -->
    {{--<section id="content" style="background: #F5F5F5">--}}
        {{--<div class="banner">--}}
            {{--<div class="content-wrap" style="height:250px">--}}
                {{--<img src="{{url($course->image)}}" width="100%">--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}





    <!-- Course Information
    ============================================= -->

    <section>
        <div class="content-wrap" style="padding-bottom: 0px">
            <div class="container">
                <div class="row course-detail">
                    <div class="col-md-9">
                        <dl class="dl-horizontal">
                            <dt>@if($course->icon)
                                    <img class="cicon2" src="{{url($course->icon)}}">
                                @else
                                    <img class="cicon2" src="{{url('images/logo.png')}}" alt="{{$course->name}}">
                                @endif
                            </dt>
                            <dd><h2>{{$course->name}}</h2></dd>
                            <dt>{{ trans('academy.course.cost') }}</dt>
                            <dd> {{ trans('academy.contact.forprice') }} </dd>
                            <dt></dt>
                            <dd>{!! $course->summary  !!}</dd>
                            <dt>{{ trans('academy.course.outcomes') }}</dt>
                            <dd>{!! $course->outcomes !!}</dd>
                            <dt>{{ trans('academy.course.structure') }}</dt>
                            <dd>{!! $course->structure !!}</dd>
                            <dt>{{ trans('academy.course.involved') }}</dt>
                            <dd>{!! $course->involved !!}</dd>
                        </dl>
                    </div>
                    <div class="col-md-3 private-button">
                        <a href="https://elabor8.com.au/contact/" class="button button-desc">
                        <div>{{ trans('academy.contact.us') }}</div></a>
                        @if($course->type == 'Private & Public')
                            <a href="{{url('public/'.$course->slug)}}" class="button button-desc">
                            <div>{!! trans('academy.public.dates') !!}</div></a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Trainers block
    ============================================= -->

    <section id="content" style="background: #F5F5F5">
        <div class="content-wrap">
            <div class="container">

                <h3>{{ trans('academy.course.trainers') }}</h3>

                <div class="row partners">
                    @foreach($course->trainers->sortBy('sort') as $trainer)
                        @if($trainer->published ==  true)
                        <div class="col-sm-6 col-md-1-5 col-lg-1-5" style="text-align: center">
                            <a style="color:#424242;"  href="{{route('trainer', $trainer->slug)}}">
                                @if($trainer->image)
                                    <img src="{{url($trainer->image)}}" alt="{{$trainer->name}}" title="{{$trainer->name}}" style="padding:0px 15px">
                                @endif
                            <p>{{$trainer->name}}</p>
                            </a>
                        </div>
                        @endif
                    @endforeach


                </div>
            </div>
        </div>
    </section>



    <!-- Testimonials block
    ============================================= -->
  @if($course->testimonials->count() > 0)
    <section id="content" style="background: #fff;">
        <div class="content-wrap">
            <div class="container">
                <div class="col-md-12">
                    <h3>{{ trans('academy.attendees.saying') }}</h3>

                    <div class="fslider testimonial noborder noshadow" data-animation="slide" data-arrows="true" data-speed="1000" data-pause="10000">
                        <div class="flexslider">
                            <div class="slider-wrap">
                                @foreach($course->testimonials->sortBy('sort') as $testimonial)
                                    <div class="slide">
                                        <div class="testi-image">
                                            @if($testimonial->image)
                                                <a href="#"><img src="{{url($testimonial->image)}}" alt="Customer Testimonails"></a>
                                            @endif
                                        </div>
                                        <div class="testi-content">
                                            <p><em>{{$testimonial->feedback}}</em></p>
                                            <div class="testi-meta">
                                                <b>{{$testimonial->name}}</b>, {{$testimonial->title}}, {{$testimonial->company}}

                                                @foreach($testimonial->courses as $course)
                                                    @if($course->type == 'Private')
                                                        <span><a href="{{url('private/'.$course->slug)}}">{{$course['name']}}</a></span>
                                                    @else
                                                        <span><a href="{{url('public/'.$course->slug)}}">{{$course['name']}}</a></span>
                                                    @endif
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    @endif


    <!-- Call to action block
        ============================================= -->
    @if($course->testimonials->count() > 0)
    <section id="content" style="background: #F5F5F5; text-align:center">
        @include('layouts.callto')
    </section>
    @else
        <section id="content" style="background: #fff; text-align:center">
            @include('layouts.callto')
        </section>
    @endif




@endsection

@section('scripts')

@endsection


