@yield('testimonial')

<div class="row partners">
    @foreach($trainers as $trainer)
        @if($trainer->image)
        <div class="col-sm-6 col-md-1-5 col-lg-1-5" style="text-align: center">
            <a style="color:#424242;"  href="{{route('trainer', $trainer->slug)}}">
                <img src="{{$trainer->image}}" alt="{{$trainer->name}}" title="{{$trainer->name}}" style="padding:0px 15px; max-height: 165px;">
                <p>{{$trainer->name}}</p>
            </a>
        </div>
        @endif
    @endforeach
</div>
