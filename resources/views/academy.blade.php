@extends('layouts.app')

@section('meta')
    <title>Academy</title>
    <meta name="description" content="">
@endsection

@section('content')

    <!-- Banner
    ============================================= -->
    <section id="content" style="background: #F5F5F5">
        <div class="banner">

            <div class="content-wrap">

                <div class="container clearfix">
                    <div class="col-md-6">
                        <div class="bannerleft">
                            <h1>{{$banner->title}}</h1>
                            <p>{{$banner->message}}</p>
                            <a href="{{route('courses.public')}}" class="button button-desc2" >
                                <div>{{ trans('academy.public.courses') }}</div></a>
                            <a href="{{route('courses.private')}}" class="button button-desc2">
                                <div>{{ trans('academy.private.courses') }}</div></a>
                        </div>
                    </div>
                    <div class="col-md-6" style="padding: 0px; margin: 0px">
                        <img src="{{url($banner->image)}}" alt="{{$banner->alt}}" title="{{$banner->title}}">
                    </div>

                </div>

            </div>

        </div>
    </section>


<!-- Partner Block
    ============================================= -->
    <section id="content">
        <div class="content-wrap">

            <div class="container">
                    @foreach($partners as $partner)
                        <div class="col-md-2">
                            <a href="{{$partner->website}}" target="_blank">
                                <img src="{{$partner->image}}" alt="{{$partner->name}}" title="{{$partner->title}}" style="padding:0px 0px">
                            </a>
                        </div>
                    @endforeach
            </div>

            <div class="container">
                <div class="row partners" style="text-align: center; margin-top: 20px;">

                    <h1>{{ trans('academy.people.trained') }}</h1>

                    @foreach($clients as $client)
                        <div class="col-md-2">
                            <a href="{{$client->website}}" target="_blank">
                                <img src="{{$client->logo}}" alt="{{$client->name}}" title="{{$client->title}}" style="padding:0px 10px">
                            </a>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
    </section>


<!-- We have trained block
    ============================================= -->

    {{--<section id="content" style="background: #F5F5F5;">--}}
        {{--<div class="content-wrap">--}}
            {{--<div class="container">--}}
                {{--<div class="row partners">--}}
                    {{--@foreach($clients as $client)--}}
                    {{--<div class="col-md-2"><img src="{{$client->logo}}" alt="{{$client->name}}" style="padding:0px 35px"></div>--}}
                    {{--@endforeach--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}


<!-- Buttons block
        ============================================= -->
    <section id="content" style="text-align:center; background: #F5F5F5;">
        <div class="content-wrap">
            <div class="container">
                <a href="{{route('courses.public')}}" class="button button-desc" >
                    <div>{{ trans('academy.public.courses') }}</div></a>
                <a href="{{route('courses.private')}}" class="button button-desc" style="background:#">
                    <div>{{ trans('academy.private.courses') }}</div></a>
                <h3 style="margin: 30px 0px 0px 0px"><a style="color:#000" href="https://elabor8.com.au/contact/">{{ trans('academy.getin.touch') }}</a></h3>
            </div>
        </div>
    </section>


<!-- Testimonials block
    ============================================= -->

    <section id="content">
        <div class="content-wrap">
            @include('layouts.testimonial')
        </div>
    </section>


<!-- Trainers block
    ============================================= -->

    <section id="content" style="background: #F5F5F5;">
        <div class="content-wrap">
            <div class="container">
                <h2>{{ trans('academy.our.trainers') }}</h2>
                @include('layouts.trainers-block')
            </div>
        </div>
    </section>



@endsection

@section('scripts')

    <script type="text/javascript">
        $(document).ready(function(){
        $('.your-class').slick({
        setting-name: setting-value
        });
        });
    </script>

@endsection


