<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{

    use Sluggable;

    protected $fillable = [
        'name', 'image', 'bio', 'slug'
    ];

    //
    public function courses(){
        return $this->belongsToMany('App\Course', 'course_trainer',
            'trainers_id', 'courses_id' );
    }


    public function sluggable()

    {

        return [

            'slug' => [

                'source' => 'name'

            ]

        ];

    }


}
