@extends('layouts.app')

@section('meta')
    <title>Trainers</title>
    <meta name="description" content="">
@endsection

@section('content')

    <!-- Banner
    ============================================= -->
    <section id="content" style="background: #F5F5F5">
        @include('layouts.banner')
    </section>


    <!-- Trainers block
    ============================================= -->

    <section id="content">
        <div class="content-wrap">
            <div class="container">

                <div class="row partners">
                    @foreach($trainers as $trainer)
                        <div class="col-sm-6 col-md-1-5 col-lg-1-5" style="text-align: center">
                           <a style="color:#424242;"  href="#{{$trainer->name}}">
                               <img src="{{$trainer->image}}" alt="{{$trainer->name}}" style="padding:0px 15px">
                               <p>{{$trainer->name}}</p>
                           </a>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>

    </section>


    <!-- Trainers List
    ============================================= -->

    <section id="content"  style="background: #F5F5F5;">
        <div class="content-wrap">
            <div class="container">

                    @foreach($trainers as $trainer)
                    <div class="row trainers">
                        <a name="{{$trainer->name}}"></a>
                    <div class="col-md-3">
                            <img src="{{$trainer->image}}" alt="{{$trainer->name}}" style="padding:0px 15px">
                        </div>
                        <div class="col-md-6">
                            <h3>{{$trainer->name}}</h3>
                            <p>{{$trainer->bio}}</p>
                        </div>
                        <div class="col-md-3">
                            <h4 style="margin: 10px 0px 30px">Certified to train in:</h4>
                            <ul style="padding-left: 13px">
                                @foreach($trainer->courses as $course)
                                    @if($course->type == 'Private')
                                        <li><a style="color:#424242;" href="private/{{$course->id}}">{{$course['name']}}</a></li>
                                    @else
                                        <li><a style="color:#424242;" href="public/{{$course->id}}">{{$course['name']}}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>

    </section>

@endsection

@section('scripts')

@endsection


