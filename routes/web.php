<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/public', 'CourseController@getPublic')->name('courses.public');
Route::get('/private', 'CourseController@getPrivate')->name('courses.private');

Route::get('/public/{slug}', 'CourseController@getCourseDetail')->name('courses.detail');

Route::get('/academy/{slug}', 'PageController@getPage')->name('page');


Route::get('/private/{slug}', 'CourseController@getCourseDetailPrivate')->name('courses.detail-private');

Route::get('/', 'CourseController@getAcademy')->name('academy');

Route::get('/academy', 'CourseController@getAcademy')->name('academy');
Route::get('/load_more', 'CourseController@getDatesLoadMore')->name('loadmore');;


Route::get('/trainers', 'TrainerController@getTrainers')->name('trainers.all');

Route::get('/trainer/{id}', 'TrainerController@getTrainer')->name('trainer');
Route::get('/trainer/{slug}', 'TrainerController@getTrainer')->name('trainer');


Route::get('/get', 'CourseController@get')->name('get');

Route::get('/courses-update', 'CourseController@coursesUpdate')->name('coursesupdate');
Route::get('/dates-update', 'CourseController@datesUpdate')->name('datesupdate');
