<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePivotTableTrainers2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('course_trainer', function (Blueprint $table) {
            //
            $table->dropColumn('course_id');
            $table->dropColumn('trainer_id');
            $table->integer('courses_is');
            $table->integer('trainers_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_trainer', function (Blueprint $table) {
            //
        });
    }
}
