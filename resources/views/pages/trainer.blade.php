@extends('layouts.app')

@section('meta')
    <title>{{$trainer->title_tag ? $trainer->title_tag : $trainer->name}}</title>
    <meta name="description" content="{{$trainer->meta_description ? $trainer->meta_description : $trainer->bio }}">
@endsection

@section('content')

    <!-- Trainers block
    ============================================= -->

    <section id="content"  style="background: #fffff;">
        <div class="content-wrap">
            <div class="container">

                    <div class="row trainers">
                        <a name="{{$trainer->name}}"></a>
                        <div class="col-md-3">
                            <img src="{{url($trainer->image)}}" alt="{{$trainer->name}}" title="{{$trainer->name}}" style="padding:0px 15px">
                        </div>
                        <div class="col-md-6">
                            <h3>{{$trainer->name}}</h3>
                            <p>{!! $trainer->bio !!}</p>
                            @if($trainer->linkedin)
                            <p><a href="{{$trainer->linkedin}}" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true" style="font-size: 22px"></i></a></p>
                            @endif
                        </div>
                        <div class="col-md-3">
                            <h4 style="margin: 10px 0px 30px">Certified to train in:</h4>
                            <ul style="padding-left: 13px">
                                @foreach($trainer->courses as $course)
                                    @if($course->type == 'Private')
                                        <li><a style="color:#424242;" href="{{url('private/'.$course->slug)}}">{{$course['name']}}</a></li>
                                    @else
                                        <li><a style="color:#424242;" href="{{url('public/'.$course->slug)}}">{{$course['name']}}</a></li>
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>

            </div>
        </div>
        </div>

    </section>

@endsection

@section('scripts')

@endsection


