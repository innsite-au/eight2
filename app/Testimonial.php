<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{
    //
    protected $fillable = [
        'name', 'title',  'company', 'image', 'feedback'
    ];

    //
    public function courses(){
        return $this->belongsToMany('App\Course', 'course_testimonial',
            'testimonials_id', 'courses_id');
    }
}
