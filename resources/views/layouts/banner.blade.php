@yield('banner')


<div class="banner">

    <div class="content-wrap">

        <div class="container clearfix">
            <div class="col-md-6">
                <div class="bannerleft">
                    <h1>{{$banner->title}}</h1>
                    <p>{{$banner->message}}</p>
                </div>
            </div>
            <div class="col-md-6" style="padding: 0px; margin: 0px">
                <img src="{{url($banner->image)}}" alt="{{$banner->alt}}" title="{{$banner->title}}">
            </div>

        </div>

    </div>

</div>