<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{
    protected $fillable = [
        'course_id', 'code', 'name', 'start_date', 'end_date', 'is_full', 'places_remaining', 'register_uri',
        'is_private', 'city', 'event_id'
    ];


    protected $dates = [
        'start_date',
        'end_date',
    ];

    public function course(){
        return $this->hasOne('App\Course', 'event_id', 'course_id');
    }

}
