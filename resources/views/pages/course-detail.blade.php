@extends('layouts.app')

@section('meta')
    <title>{{$course->title_tag ? $course->title_tag : $course->name}}</title>
    <meta name="description" content="{{$course->meta_description ? $course->meta_description : $course->summary }}">
@endsection

@section('content')




    <!-- Banner
    ============================================= -->
    {{--<section id="content" style="background: #F5F5F5">--}}
        {{--<div class="banner">--}}
            {{--<div class="content-wrap" style="height:250px">--}}
                  {{--<img src="{{url($course->image)}}" width="100%">--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}


    <!-- Course Information
    ============================================= -->

    <section>
        <div class="content-wrap" style="padding-bottom: 0px">
            <div class="container">
                <div class="row course-detail">
                    <div class="col-md-9">
                        <dl class="dl-horizontal">
                            <dt>@if($course->icon)
                                    <img class="cicon2" src="{{url($course->icon)}}">
                                @else
                                    <img class="cicon2" src="{{url('images/logo.png')}}" alt="{{$course->name}}">
                                @endif
                            </dt>
                            <dd><h2>{{$course->name}}</h2></dd>
                            <dt>{{ trans('academy.course.cost') }}</dt>
                            <dd>${{$course->price}} {{ trans('academy.inc.gst') }}</dd>
                            <dt></dt>
                            <dd>{!! $course->summary  !!}</dd>
                            <dt>{{ trans('academy.course.outcomes') }}</dt>
                            <dd>{!! $course->outcomes !!}</dd>
                            <dt>{{ trans('academy.course.structure') }}</dt>
                            <dd>{!! $course->structure !!}</dd>
                            <dt>{{ trans('academy.course.involved') }}</dt>
                            <dd>{!! $course->involved !!}</dd>
                        </dl>
                    </div>
                    <div class="col-md-3">
                        <h3 style="padding: 10px 0px">{{ trans('academy.course.dates') }}</h3>
                        @if(count($course->dates) > 0)
                        @foreach($course->dates as $date)
                            @if(\Carbon\Carbon::parse($date->start_date) > \Carbon\Carbon::today())
                            <div class="dates">
                                <div class="date">
                                    {{$date->city}}<br/>
                                    {{ Carbon\Carbon::parse($date->start_date)->format('j M Y') }}
                                </div>
                                <div class="book">
                                    <a href="{{$date->register_uri}}" target="_blank" class="button button-small">{{ trans('academy.book.now') }}</a>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                            @endif
                        @endforeach
                        @else
                            {{ trans('academy.no.courses') }}
                        @endif


                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Trainers block
    ============================================= -->

    <section id="content" style="background: #F5F5F5">
        <div class="content-wrap">
            <div class="container">

                <h3>{{ trans('academy.course.trainers') }}</h3>

                <div class="row partners">
                    @foreach($course->trainers->sortBy('sort') as $trainer)
                        @if($trainer->published ==  true)
                        <div class="col-sm-6 col-md-1-5 col-lg-1-5" style="text-align: center">
                            <a style="color:#424242;"  href="{{route('trainer', $trainer->slug)}}">
                            <img src="{{url($trainer->image)}}" alt="{{$trainer->name}}" title="{{$trainer->name}}" style="padding:0px 15px">
                            <p>{{$trainer->name}}</p>
                            </a>
                        </div>
                        @endif
                    @endforeach

                </div>
            </div>
        </div>
    </section>



    <!-- Testimonials block
    ============================================= -->

    <section id="content" style="background: #fff;">
        <div class="content-wrap">
            <div class="container">


           <div class="fslider testimonial noborder noshadow" data-animation="slide" data-arrows="true" data-speed="1000" data-pause="10000">
            <div class="flexslider">
                <div class="slider-wrap">
                    @foreach($course->testimonials->sortBy('sort') as $testimonial)
                        <div class="slide">
                            <div class="testi-image">
                                @if($testimonial->image)
                                    <a href="#"><img src="{{url($testimonial->image)}}" alt="Customer Testimonails"></a>
                                @endif
                            </div>
                            <div class="testi-content">
                                <p><em>{{$testimonial->feedback}}</em></p>
                                <div class="testi-meta">
                                    <b>{{$testimonial->name}}</b>, {{$testimonial->title}}, {{$testimonial->company}}

                                    @foreach($testimonial->courses as $course)
                                        @if($course->type == 'Private')
                                            <span><a href="{{url('private/'.$course->slug)}}">{{$course['name']}}</a></span>
                                        @else
                                            <span><a href="{{url('public/'.$course->slug)}}">{{$course['name']}}</a></span>
                                        @endif
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
            </div>


        </div>
        </div>



    </section>


    <!-- Call to action block
        ============================================= -->
    <section id="content" style="background: #F5F5F5; text-align:center">
        @include('layouts.callto')
    </section>




@endsection

@section('scripts')

@endsection


