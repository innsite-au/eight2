<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PageController extends Controller
{
    public function getPage($slug) {
        $page = Page::where('slug', '=', $slug)->first();

        return view('pages.page', compact('page'));


    }
}
