<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="SemiColonWeb"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('meta')

    <!-- Styles -->
    <link href="{{ URL::asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="/css/bootstrap.min.css">


    <!-- Stylesheets
            ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('theme/css/bootstrap.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ URL::asset('style.css')}}" type="text/css"/>
    <link rel="stylesheet" href="{{ URL::asset('theme/css/font-icons.css') }}" type="text/css"/>
    <link href="{{ URL::asset('css/jquery.nok.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('theme/css/dark.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ URL::asset('theme/css/animate.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ URL::asset('theme/css/responsive.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ URL::asset('css/rome.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ URL::asset('custom.css') }}" type="text/css"/>
    <link rel="stylesheet" href="{{ URL::asset('custom_responsive.css') }}" type="text/css"/>

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>

</head>


<body class="stretched">
<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">
    <!-- Header
    ============================================= -->
    <header id="header" class="">
        <div id="header-wrap">
            <div class="container clearfix">
                <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>
                <!-- Logo
                ============================================= -->
                <div id="logo">
                    <a href="/" class="standard-logo" data-dark-logo="{{ url('/images/Elabor8-Logo-2016.png') }}"><img src="{{ url('/images/Elabor8-Logo-2016.png') }}" height="80px" alt="elabor8"></a>
                    <a href="/" class="retina-logo" data-dark-logo="{{ url('/images/Elabor8-Logo-2016.png') }}"><img src="{{ url('/images/Elabor8-Logo-2016.png') }}" height="80px" alt="elabor8"></a>
                </div><!-- #logo end -->
                <!-- Primary Navigation
                ============================================= -->
                <nav id="primary-menu">

                    <ul>
                        <li><a href="https://elabor8.com.au/about/">Company</a></li>
                        <li><a href="https://elabor8.com.au/approach/">Services</a></li>
                        <li><a href="#">Academy</a></li>
                        <li><a href="https://elabor8.com.au/blog/">Resources</a></li>
                        <li><a href="https://elabor8.com.au/careers/">Careers</a></li>
                        <li><a href="https://elabor8.com.au/contact/">Contact</a></li>
                    </ul>

                    <!-- Top Cart
                    ============================================= -->
                {{--<div id="top-cart">--}}

                {{--</div>--}}
                <!-- #top-cart end -->

                    <!-- Top Search
                    ============================================= -->
                {{--<div id="top-search">--}}
                {{--<a href="#" id="top-search-trigger"><i class="icon-search3"></i><i class="icon-line-cross"></i></a>--}}
                {{--<form action="search.html" method="get">--}}
                {{--<input type="text" name="q" class="form-control" value="" placeholder="Type &amp; Hit Enter..">--}}
                {{--</form>--}}
                {{--</div>--}}
                <!-- #top-search end -->
                </nav><!-- #primary-menu end -->
            </div>
        </div>
    </header>
    <!-- #header end -->


    @yield('content')
</div>
</div>

<!-- Footer
		============================================= -->
<footer id="footer" class="dark">

    <!-- Copyrights
    ============================================= -->
    <div id="copyrights">

        <div class="container clearfix">

            <div class="col_half">
                <span style="padding-right:20px">Home</span> Blog
            </div>

            <div class="col_half col_last tright">
                <div class="copyrights-menu copyright-links fright clearfix">
                    Copyright © Elabor8
                </div>
            </div>

        </div>

    </div><!-- #copyrights end -->

</footer><!-- #footer end -->

<!-- Scripts -->
<script src="/js/app.js"></script>
<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>
<!-- External JavaScripts
============================================= -->
<script src="https://use.fontawesome.com/50ecaac621.js"></script>
<script type="text/javascript" src="{{ URL::asset('theme/js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('theme/js/plugins.js') }}"></script>
<script src='{{ URL::asset('js/jquery.nok.min.js') }}'></script>
<script src='{{ URL::asset('js/moment.js') }}'></script>
<script src='{{ URL::asset('js/rome.standalone.min.js') }}'></script>
<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="{{ URL::asset('theme/js/functions.js') }}"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
@yield('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('.multiplecheckboxes').multiselect();
    });
</script>
</body>
</html>
