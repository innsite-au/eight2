@extends('layouts.app')

@section('meta')
    <title>{{$meta->title}}</title>
    <meta name="description" content="{{ $meta->description }}">
@endsection

@section('content')

    <!-- Banner
    ============================================= -->
    <section alt="{{ $page->alt_tag }}" title="{{ $page->alt_tag }}" id="content ban"
             style="
                     background-image: url({{ $page->banner_image }});
                     background-position: center center;
                     background-size: cover;
                     ">
        <div class="upb_bg_overlay"></div>
        <div class="upb_bg_overlay_pattern"></div>
        <div class="banner">
            <div class="content-wrap">

                <div class="container clearfix">
                    <div class="col-md-12">
                        <div class="banner-center">
                            <h1>{{ $page->banner_title }}
                            </h1>
                            <p>{{ $page->banner_text }}</p>
                            <a href="{{route('courses.public')}}" class="button button-desc2" >
                                <div>{{ trans('academy.public.courses') }}</div></a>
                            <a href="{{route('courses.private')}}" class="button button-desc2">
                                <div>{{ trans('academy.private.courses') }}</div></a>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>


<!-- Intro & partners block
    ============================================= -->
    <section id="content" style="background: #F5F5F5;">
        <div class="content-wrap" style="padding: 60px 0px 0px">
            <div class="container">
                <div class="row">

                    {{--Intro--}}
                    <div class="col-md-8">
                        {!! $page->body !!}
                    </div>

                    {{--Partners--}}
                    <div class="col-md-4">
                        @foreach($partners as $partner)
                            @if($partner->image)
                                <div class="partner">
                                    <a href="{{$partner->website}}" target="_blank">
                                        <img src="{{$partner->image}}" alt="{{$partner->name}}" title="{{$partner->title}}">
                                    </a>
                                </div>
                            @endif
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </section>


<!-- Clients block
    ============================================= -->
    <section id="content" style="text-align:center;">
        <div class="content-wrap"  style="padding: 40px 0px 0px">
            <div class="container">
                <div class="row partners clients" style="text-align: center; margin-top: 20px;">

                    <h1>{{ trans('academy.people.trained') }}</h1>

                    @foreach($clients as $client)
                        @if($client->logo)
                        <div class="col-md-2">
                            <a href="{{$client->website}}" target="_blank">
                                <img src="{{$client->logo}}" alt="{{$client->name}}" title="{{$client->title}}" style="padding:0px 10px">
                            </a>
                        </div>
                        @endif
                    @endforeach

                </div>
            </div>
        </div>
    </section>


<!-- Buttons block
        ============================================= -->
    <section id="content" style="text-align:center;  background: #F5F5F5;">
        <div class="content-wrap">
            <div class="container">
                <a href="{{route('courses.public')}}" class="button button-desc" >
                    <div>{{ trans('academy.public.courses') }}</div></a>
                <a href="{{route('courses.private')}}" class="button button-desc" style="background:#">
                    <div>{{ trans('academy.private.courses') }}</div></a>
                <h3 style="margin: 30px 0px 0px 0px"><a style="color:#000" href="https://elabor8.com.au/contact/">{{ trans('academy.getin.touch') }}</a></h3>
            </div>
        </div>
    </section>


    <!-- Dates Block
============================================= -->
    <section id="content">
        <div class="content-wrap">
            <div class="container">
                @include('layouts.dates')
            </div>
        </div>
    </section>


<!-- Testimonials block
    ============================================= -->

    <section id="content" style="background: #F5F5F5;">
        <div class="content-wrap">
            @include('layouts.testimonial')
        </div>
    </section>


<!-- Trainers block
    ============================================= -->

    <section id="content" >
        <div class="content-wrap">
            <div class="container">
                <h2>{{ trans('academy.our.trainers') }}</h2>
                @include('layouts.trainers-block')
            </div>
        </div>
    </section>



@endsection

@section('scripts')

    <script type="text/javascript">
        $(document).ready(function(){
        $('.your-class').slick({
        setting-name: setting-value
        });
        });
    </script>

@endsection


