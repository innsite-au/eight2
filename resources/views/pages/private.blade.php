@extends('layouts.app')

@section('meta')
    <title>{{ $meta->title }}</title>
    <meta name="description" content="{{ $meta->description }}">
@endsection

@section('content')

    <!-- Banner
    ============================================= -->
    <section id="content ban"  alt="{{ $page->alt_tag }}" title="{{ $page->alt_tag }}"
             style="
                     background-image: url({{ $page->banner_image }});
                     background-position: center center;
                     background-size: cover;
                     ">
        <div class="upb_bg_overlay"></div>
        <div class="upb_bg_overlay_pattern"></div>
        <div class="banner">
            <div class="content-wrap">

                <div class="container clearfix">
                    <div class="col-md-12">
                        <div class="banner-center">
                            <h1>{{ $page->banner_title }}</h1>
                            <p>{{ $page->banner_text }}</p>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section>


    <!-- Intro block
   ============================================= -->
    <section id="content">
        <div class="content-wrap" style="padding: 60px 0px 0px">
            <div class="container">
                <div class="row">

                    {{--Intro--}}
                    <div class="col-md-12">
                        {!! $page->body !!}
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section id="content">

        <div class="content-wrap">
            <div class="container" style="text-align: center">

                @foreach($categories as $category)
                    @if($category->courses->count() > 0)
                    <div class="category_name" style="float: left">
                        <a href="#{{ $category->category_name }}" class="button button-small">{{ $category->category_name }}</a>
                    </div>
                    @endif
                @endforeach

            </div>
        </div>


    </section>

    <section id="content">

            <div class="content-wrap">
                <div class="container">

                @foreach($categories as $category)

                    @if($category->courses->count() > 0)
                    <div class="col-md-12" style="border-bottom: 1px solid #ccc">
                        <a name="{{$category->category_name}}"></a>
                        <h3>{{$category->category_name}}</h3>

                        @foreach($category->courses as $course)
                            @if($course->published)

                                <div class="row private-courses">
                                    <div class="col-md-2 course-icon">
                                        @if($course->icon)
                                            <img src="{{$course->icon}}" alt="{{$course->name}}"  title="{{$course->name}}">
                                        @else
                                            <img src="{{url('images/logo.png')}}" alt="{{$course->name}}"  title="{{$course->name}}">
                                        @endif
                                    </div>
                                    <div class="col-md-8 course-text">
                                        <h3>{{$course->name}}</h3>
                                        <p>{!!   $course->short_description !!}</p>
                                    </div>
                                    <div class="col-md-2 course-button">
                                        <a href="{{url('/private/'.$course->slug)}}" class="button button-small">{{ trans('academy.view.course') }}</a>
                                    </div>
                                </div>

                            @endif
                        @endforeach

                    </div>
                    @endif

                @endforeach

                </div>
            </div>

    </section>



    <!-- Call to action block
        ============================================= -->
    <section id="content" style="background: #F5F5F5; text-align:center">
        @include('layouts.callto')
    </section>




@endsection

@section('scripts')

@endsection


