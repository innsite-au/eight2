@extends('layouts.app')

@section('meta')
    <title>{{$page->title ? $page->title : $page->name}}</title>
    <meta name="description" content="{{ $page->meta_desc }}">
@endsection

@section('content')

    <!-- Banner
    ============================================= -->
    @if($page->banner_image)
        <section id="content" style="background: #F5F5F5">
        <div class="banner">
            <div class="content-wrap">

                <div class="container clearfix">
                    @if($page->banner_title)
                    <div class="col-md-6">
                        <div class="bannerleft">
                            <h1>{{$page->banner_title}}
                            </h1>
                            <p>{{$page->banner_text}}</p>
                        </div>
                    </div>
                    <div class="col-md-6" style="padding: 0px; margin: 0px">
                        <img src="{{url($page->banner_image)}}" alt="{{$page->banner_title}}" title="{{$page->banner_title}}">
                    </div>
                    @else
                        <div class="col-md-12" style="padding: 0px; margin: 0px">
                            <img src="{{url($page->banner_image)}}" alt="{{$page->banner_title}}" title="{{$page->banner_title}}">
                        </div>
                    @endif

                </div>

            </div>

        </div>
    </section>
    @endif


    <!-- Page Body
        ============================================= -->
    <section id="content">
        <div class="content-wrap">

            <div class="container">

                {!! $page->body !!}

            </div>


        </div>
    </section>



@endsection




