<h3 style="text-align: center">{{ trans('academy.book.into') }}</h3>
<div class="row partners">


    <div class="table-responsive">
        <table class="table table-hover desk">
            <tr style="background: #071c2c; color: #fff" class="desk">
                <th width="30%">Date</th>
                <th width="28%">Name</th>
                <th width="10%">City</th>
                <th width="15%">Price</th>
                <th width="20%"></th>
            </tr>
        </table>
        <table class="table table-hover desk desk1">
        </table>
    </div>

    <div class="table-responsive">
        <table class="table table-hover mob">
            <tr style="background: #071c2c; color: #fff" class="mob">
                <th width="20%">Date</th>
                <th width="30%">Name</th>
                <th width="15%">City</th>
                <th width="15%">Price</th>
                <th width="20%"></th>
            </tr>
        </table>
        <table class="table table-hover mob" width="100%">

        </table>
    </div>

    <div class="text-center">
        <a href="javascript:void(0);" onclick="loadMore()" class="button button-small">Load More</a>
    </div>
</div>

<script src="theme/js/jquery.js"></script>
<script>
    var numberOfResults = 0;

    $(function() {
        loadMore();
    });
    function loadMore() {
        console.log(numberOfResults);
        numberOfResults += 5;
        // $('.loader').show();
        $.ajax({
            type: 'GET',
            url: 'load_more',
            data: {numberOfResults: numberOfResults},
            success: function(data) {
                var date = data.data;
                // if (data.to == data.total) {
                //     $.nok({message: "No more courses found"});
                // }
                if (date.length != 0) {
                    $('.desk1').empty();
                    $('.mob').empty();
                    $.each(date, function(i, item) {
                        if (item) {
                            var start_date = moment(item.start_date).format("D MMM YYYY");
                            var end_date = moment(item.end_date).format("D MMM YYYY");
                            console.log(start_date);
                            var price = item.course.price;
                            var link = '/public/' + item.course.slug;
                            var style = 'text-align: center';
                            var className = 'button button-small';
                            $('.desk1').append('<tr><td>' + start_date +' - ' +  end_date + '</td><td>' + item.name + '</td><td>' + item.city + '</td><td>$' + price + ' Inc GST</td><td style='+ style +'><a href='+ link +' class='+ className +'>More Information</a></td></tr>');
                            $('.mob').append('<tr><td>' + start_date +' - ' +  end_date + '<br/>' + item.name + '<br/>' + item.city + '<br/>$' + price + ' Inc GST<br/><a href='+ link +' class='+ className +'>More Information</a></td></tr>');

                        }
                    });
                }
            }
        });
    }
</script>