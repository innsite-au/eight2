@yield('callto')

<div class="content-wrap">

    <div class="container">
        <h2>{{ trans('academy.call.title') }}</h2>
        <a href="https://elabor8.com.au/contact/" class="button button-desc" style="padding: 24px 64px;">
            <div>{{ trans('academy.call.button') }}</div></a>
        <span class="or">Or</span>
        <a href="https://elabor8.com.au/contact/" style="color:#071c2c"><span class="call">{{ trans('academy.call.text') }}</span></a>
    </div>

</div>