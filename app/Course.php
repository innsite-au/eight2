<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{

    protected $fillable = [
        'event_id', 'code', 'name', 'image', 'logo', 'price', 'summary', 'image', 'type',
        'outcomes', 'structure', 'involved',
    ];

    //
    public function trainers(){
        return $this->belongsToMany('App\Trainer', 'course_trainer',
            'courses_id', 'trainers_id');
    }

    //
    public function categories(){
        return $this->belongsToMany('App\Category');
    }

    //
    public function testimonials(){
        return $this->belongsToMany('App\Testimonial', 'course_testimonial',
            'courses_id', 'testimonials_id');
    }

    //
    public function dates(){
        return $this->hasMany('App\Date', 'course_id', 'event_id');
    }

}
