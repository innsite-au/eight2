@yield('testimonial')

<div class="container">

    <div class="col-md-2"></div>
    <div class="col-md-8">

        <h2>{{ trans('academy.attendees.saying') }}</h2>

        <div class="fslider testimonial noborder noshadow" data-animation="slide" data-arrows="true" data-speed="1000" data-pause="10000">
            <div class="flexslider">
                <div class="slider-wrap">
                    @foreach($testimonials as $testimonial)
                        <div class="slide">
                            <div class="testi-image">
                                @if($testimonial->image)
                                <img src="{{url($testimonial->image)}}" alt="Customer Testimonails">
                                @endif
                            </div>
                            <div class="testi-content">
                                <p><em>{{$testimonial->feedback}}</em></p>
                                <div class="testi-meta">
                                    <b>{{$testimonial->name}}</b>, {{$testimonial->title}}, {{$testimonial->company}}

                                    @foreach($testimonial->courses as $course)
                                        @if($course->type == 'Private')
                                            <span><a href="private/{{$course->slug}}">{{$course->name}}</a></span>
                                            @else
                                            <span><a href="public/{{$course->slug}}">{{$course->name}}</a></span>
                                        @endif
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>

    </div>
    <div class="col-md-2"></div>


</div>
